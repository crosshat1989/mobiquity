import { Component, OnInit, ViewChild } from '@angular/core';
import { F1Service } from '../../../core/services/core.service';
import * as _ from 'lodash';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { SeasonComponent } from "../season/season.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [F1Service],
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
data: any;
years: any[] = _.range(2005, 2016);
  constructor(private f1Service: F1Service, public dialog: MatDialog, private router: Router) { }

setYear(year) {
  this.f1Service.getSeason(year).subscribe((data: any) => {
      this.data = data;
      console.log(data);
    })
}

openDialog(year, winner): void {
  console.log(year, winner);
    let dialogRef = this.dialog.open(SeasonComponent, {
      data: {
        'season': year,
        'winner': winner
      },
      width: '80%',
      height: '80%'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

logout() {
    localStorage.removeItem("login");
    this.router.navigate(['login']);
}

  ngOnInit() {
    this.f1Service.getWinner().subscribe((data: any) => {
      this.data = data;
    })
  }

}
